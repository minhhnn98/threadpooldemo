package com.nhatminh.example.concurrency.threadpool;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Ref: https://ojiofong.github.io/tech/2016/08/29/android-thread-pool/
 */

public class MainActivity extends AppCompatActivity {

    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    private static final int KEEP_ALIVE_TIME = 1000;

    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.MILLISECONDS;

    private int count = 0;

    Button btSingle;
    Button btPool;
    TextView tvProgress;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // Do some work that takes 50 milliseconds
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // Update the UI with progress
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    count++;
                    String msg = count < 100 ? "working " : "done ";
                    updateStatus(msg + count);
                }
            });

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupView();
    }

    private void setupView(){
        btPool = findViewById(R.id.btPool);
        btSingle = findViewById(R.id.btSingle);
        tvProgress = findViewById(R.id.text);

        btPool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count = 0;
                ThreadPoolExecutor mThreadPoolExecutor = new ThreadPoolExecutor(
                        NUMBER_OF_CORES + 5,   // Initial pool size
                        NUMBER_OF_CORES + 8,   // Max pool size
                        KEEP_ALIVE_TIME,       // Time idle thread waits before terminating
                        KEEP_ALIVE_TIME_UNIT,  // Sets the Time Unit for KEEP_ALIVE_TIME
                        new LinkedBlockingDeque<Runnable>());  // Work Queue

                for (int i = 0; i < 100; i++) {
                    mThreadPoolExecutor.execute(runnable);
                }
            }
        });

        btSingle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count = 0;
                Executor mSingleThreadExecutor = Executors.newSingleThreadExecutor();

                for (int i = 0; i < 100; i++) {
                    mSingleThreadExecutor.execute(runnable);
                }
            }
        });
    }


    private void updateStatus(String msg) {
        tvProgress.setText(msg);
    }
}
